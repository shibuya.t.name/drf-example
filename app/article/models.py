import traceback
from django.dispatch import receiver
from django.db import models
from django.utils.html import mark_safe
from django.utils.translation import ugettext_lazy as _

from config import settings

class Category(models.Model):
    name = models.CharField(_("カテゴリー名"), max_length=50)
    color = models.CharField('色(16進数)', max_length=7, default='#000000')
    created_at = models.DateTimeField(_("投稿日"), auto_now_add=True)
    updated_at = models.DateTimeField(_("更新日"), auto_now=True)
    
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name="カテゴリ"
        verbose_name_plural="カテゴリ"
        db_table = 'category'



class Article(models.Model):
    id = models.AutoField(_("ID"), primary_key=True)
    status_list = [("公開","公開"), ("非公開","非公開"), ("下書き", "下書き")]
    thumbnail = models.ImageField(_('サムネイル画像'), upload_to='thumbnail/%Y/%m/%d/', null=True, blank=True)
    title = models.CharField(_("タイトル"), max_length=50)
    category = models.ManyToManyField(Category, _("カテゴリー"))
    main_text = models.TextField(_("本文"))
    sub_text = models.TextField(_("説明"), null=True, blank=True)
    status = models.CharField(_("状態"), choices=status_list, max_length=50)
    created_at = models.DateTimeField(_("投稿日"), auto_now_add=True)
    updated_at = models.DateTimeField(_("更新日"), auto_now=True)
    
    def __str__(self):
        return self.title
    
    def admin_og_image(self):
        if self.thumbnail:
            return mark_safe('<img src="{}" style="width:100px;height:auto;">'.format(settings.MEDIA_URL+str(self.thumbnail)))
        else:
            return "no image"
    admin_og_image.allow_tags = True

    class Meta:
        verbose_name="記事"
        verbose_name_plural="記事"
        db_table = 'articles'
