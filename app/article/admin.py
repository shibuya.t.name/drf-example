from django.contrib import admin


""" Model """
from article.models import Article, Category

class ArticleAdmin(admin.ModelAdmin):
    readonly_fields = ("id", "created_at", "updated_at")
    fieldsets = (
        ("入力用フィールド", {'fields': (
            "title",
            "category",
            "main_text",
            "sub_text",
            "thumbnail",
            "status",
        )}),
        ("参照用フィールド", {'fields': (
            "id",
            "created_at",
            "updated_at",
        )}),
    )
    list_display = [
        "id",
        "title",
        "sub_text",
        "created_at",
        "updated_at",
        "status",
        "admin_og_image",
    ]
    list_display_links = [
        "id",
        "title",
        "sub_text",
        "created_at",
        "updated_at",
        "status",
        "admin_og_image",
    ]
    ordering = ('-created_at',)
    list_filter = ('status',)
    def image(self, instance):
        return instance
    
class CategoryAdmin(admin.ModelAdmin):
    readonly_fields = ("id", "created_at", "updated_at")
    fields = [
        "name",
        "color",
    ]
    list_display = [
        "name",
        "color",
    ]
    list_display_links = [
        "name",
        "color",
    ]
admin.site.register(Article, ArticleAdmin)
admin.site.register(Category, CategoryAdmin)