from django.urls import path
from rest_framework.routers import SimpleRouter

router = SimpleRouter()

""" Article """
from article.views import ArticleViewSet, CategoryViewSet
router.register("articles", ArticleViewSet, basename="articles")
router.register("categories", CategoryViewSet, basename="categories")


urlpatterns = router.urls

