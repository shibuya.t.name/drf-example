from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.html import mark_safe

from config import settings
from .user import User


class Profile(models.Model):
    
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="profile")
    first_name = models.CharField(_("ファーストネーム"), max_length=50)
    last_name = models.CharField(_("ラストネーム"), max_length=50)
    thumbnail = models.ImageField(_('プロフィール画像'), upload_to='profile/%Y/%m/%d/', null=True, blank=True)
    created_at = models.DateTimeField(_("作成日"), auto_now_add=True)
    updated_at = models.DateTimeField(_("更新日"), auto_now=True)

    def user_thumbnail(self):
        if self.thumbnail:
            return mark_safe('<img src="{}" style="width:100px;height:auto;">'.format(settings.MEDIA_URL+str(self.thumbnail)))
        else:
            # return mark_safe('<img src="{}" style="width:100px;height:auto;">'.format(settings.MEDIA_URL+"noimage.png"))
            return "no image"
    user_thumbnail.allow_tags = True
    
    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name="ユーザー詳細"
        verbose_name_plural="ユーザー詳細"
        db_table = 'profile'