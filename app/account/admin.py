from django.contrib import admin


from account.models.user import User
from account.admins.user import MyUserAdmin

from account.models.profile import Profile
from account.admins.profile import ProfileAdmin

admin.site.register(User, MyUserAdmin)
admin.site.register(Profile, ProfileAdmin)