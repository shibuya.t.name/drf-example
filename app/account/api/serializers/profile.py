from django.db import models
from django.db.models.deletion import CASCADE
from rest_framework import serializers

from account.models.user import User
from account.models.profile import Profile


class ProfileSerializer(serializers.ModelSerializer):
# class ProfileSerializer(serializers.HyperlinkedModelSerializer):
    # user = serializers.StringRelatedField(many=True)
    # user = serializers.PrimaryKeyRelatedField(read_only=True)
    # user = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    # user = Field(source='username')
    user = models.OneToOneField(User, related_name="user", on_delete=CASCADE)

    class Meta:
        model = Profile
        # fields = "__all__"
        fields = ['id', 'user', 'first_name', 'last_name', 'thumbnail']
        # fields = ['id', 'first_name', 'last_name', 'thumbnail', 'user']