from django.contrib.auth.models import Group, Permission
from rest_framework import serializers
# from django.contrib.auth import get_user_model

from account.models.user import User


class PermissionSerializer(serializers.ModelSerializer):    
    class Meta:
        model = Permission
        fields = "__all__"
class GroupSerializer(serializers.ModelSerializer):    
    class Meta:
        model = Group
        fields = "__all__"
        # fields = ('name',)

class UserSerializer(serializers.ModelSerializer):
  class Meta:
    model = User
    # fields = "__all__"
    read_only_fields = ('profile',)
    fields = [
        'id',
        'username',
        'email',
        'tel',
        'groups',
        'user_permissions',
        'last_login',
        'date_joined',
        'profile',
        # 'is_superuser',
        # 'password',
    ]
    # exclude = ("password",)