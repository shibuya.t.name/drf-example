""" Add """
from django.contrib.auth import get_user_model
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

""" Model """
from account.models.user import User
from django.contrib.auth.models import Group, Permission
""" Serializer """
from account.api.serializers.user import UserSerializer, GroupSerializer, PermissionSerializer



class PermissionViewSet(viewsets.ModelViewSet):
  permission_classes = (IsAuthenticated,)
  queryset = Permission.objects.all()
  serializer_class = PermissionSerializer
class GroupViewSet(viewsets.ModelViewSet):
  permission_classes = (IsAuthenticated,)
  queryset = Group.objects.all()
  serializer_class = GroupSerializer
class UserViewSet(viewsets.ModelViewSet):
  permission_classes = (IsAuthenticated,)
  queryset = User.objects.all()
  serializer_class = UserSerializer