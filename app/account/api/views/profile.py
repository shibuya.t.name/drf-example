from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated


from account.models.profile import Profile
from account.api.serializers.profile import ProfileSerializer


class ProfileViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer