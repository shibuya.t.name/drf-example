from django.urls import path
from django.views.generic import TemplateView

from rest_framework.routers import SimpleRouter

router = SimpleRouter()

""" User """
from account.api.views.user import UserViewSet, GroupViewSet, PermissionViewSet
router.register('users', UserViewSet, basename='users')
router.register('groups', GroupViewSet, basename='groups')
router.register('permissions', PermissionViewSet, basename='permissions')

from account.api.views.profile import ProfileViewSet
router.register('profiles', ProfileViewSet, basename='profiles')

urlpatterns = router.urls