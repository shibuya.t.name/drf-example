from django.contrib import admin
from django.utils.translation import ugettext_lazy as _


class ProfileAdmin(admin.ModelAdmin):
    readonly_fields = ("id", "created_at", "updated_at")
    fieldsets = (
        ("User Profile", {'fields': ('user', 'first_name',"last_name", "thumbnail")}),
    )
    # fields = [
    #     "id",
    #     "user",
    #     "first_name",
    #     "last_name",
    #     "thumbnail",
    #     "created_at",
    #     "updated_at"
    # ]
    list_display = [
        "id",
        "user",
        "first_name",
        "last_name",
        "user_thumbnail",
        "created_at",
        "updated_at"
    ]
    list_display_links = [
        "id",
        "user",
        "first_name",
        "last_name",
        "user_thumbnail",
        "created_at",
        "updated_at",
    ]
    ordering = ('-created_at',)
    # list_filter = ('status',)
    def image(self, instance):
        return instance