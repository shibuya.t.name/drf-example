from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.utils.translation import ugettext_lazy as _
from config import settings

### Models ###
from account.models.user import User

class MyUserChangeForm(UserChangeForm):
    class Meta:
        model = User
        fields = '__all__'


class MyUserCreationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('email', 'username', "tel", 'password')

class MyUserAdmin(UserAdmin):
    readonly_fields = ("id",)
    
    fieldsets = (
        (None, {'fields': ('username', 'email', "tel", 'password')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'email', "tel", 'password1', 'password2'),
        }),
    )
    form = MyUserChangeForm
    add_form = MyUserCreationForm
    list_display = ("id", 'username', 'email', "tel", 'is_staff')
    list_display_links = ("id", 'username', 'email', "tel", 'is_staff')
    list_filter = ("id", 'username', 'is_staff', 'is_superuser', 'is_active', 'groups')
    search_fields = ("id", 'username', 'email', "tel")
    ordering = ("id", 'username', 'email', "tel", )


